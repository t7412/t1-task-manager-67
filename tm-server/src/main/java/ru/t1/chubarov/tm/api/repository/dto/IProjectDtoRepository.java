package ru.t1.chubarov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.t1.chubarov.tm.dto.model.ProjectDTO;

import java.util.List;

@Repository
@Scope("prototype")
public interface IProjectDtoRepository extends IDtoRepository<ProjectDTO> {

    void deleteAll();

    void deleteByUserId(@Nullable String userId);

    void deleteById(@Nullable String Id);

    void deleteByUserIdAndId(@Nullable String userId, @Nullable String id);

    long count();

    long countAllByUserId(@Nullable String userId);

    long countAllByUserIdAndId(@Nullable String userId, @Nullable String id);

    @Nullable
    ProjectDTO findFirstById(@NotNull String id);

    @Nullable
    List<ProjectDTO> findAllByUserId(@Nullable String userId);

    @Nullable
    @Query("SELECT p FROM ProjectDTO p")
    List<ProjectDTO> findAll();

}
