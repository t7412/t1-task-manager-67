package ru.t1.chubarov.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.chubarov.tm.dto.request.DataYamlLoadFasterXmlRequest;
import ru.t1.chubarov.tm.enumerated.Role;
import ru.t1.chubarov.tm.event.ConsoleEvent;

@Component
public final class DataYamlLoadFasterXmlListener extends AbstractDataListener {

    @NotNull
    public static final String NAME = "data-load-yaml";
    @NotNull
    private final String DESCRIPTION = "Load data from yaml file.";

    @SneakyThrows
    @Override
    @EventListener(condition = "@dataYamlLoadFasterXmlListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[DATA LOAD YAML]");
        @NotNull final DataYamlLoadFasterXmlRequest request = new DataYamlLoadFasterXmlRequest(getToken());
        domainEndpoint.loadDataYamlFasterXml(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
