package ru.t1.chubarov.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.dto.model.UserDTO;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractUserResponse extends AbstractResponse {

    @Nullable
    private UserDTO user;

    @Nullable
    private String token;

    @Nullable
    private Throwable throwable;

    public AbstractUserResponse(@Nullable final UserDTO user, @Nullable final String token, @Nullable final Throwable throwable) {
        this.user = user;
        this.token = token;
        this.throwable = throwable;
    }

    public AbstractUserResponse(@Nullable final Throwable throwable) {
        this.throwable = throwable;
    }

    public AbstractUserResponse(@Nullable final String token) {
        this.token = token;
    }

    public AbstractUserResponse(@Nullable final UserDTO user) {
        this.user = user;
    }
}
