package ru.t1.chubarov.tm.api.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.enumerated.Status;

public interface IHasStatus {

    @NotNull
    String getStatus();

    void setStatus(@Nullable String status);

}
