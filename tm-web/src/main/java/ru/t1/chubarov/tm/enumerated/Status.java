package ru.t1.chubarov.tm.enumerated;


import lombok.Getter;
import org.jetbrains.annotations.Nullable;

@Getter
public enum Status {

    NOT_STARTED("Not started"),
    IN_PROGRESS("In progress"),
    COMPLETED("Completed");

    @Nullable
    private final String displayName;

    Status(@Nullable final String displayName) {
        this.displayName = displayName;
    }

    @Nullable
    public String getDisplayName() {
        return displayName;
    }

}
