package ru.t1.chubarov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Repository;
import ru.t1.chubarov.tm.model.Project;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

@Repository
public class ProjectRepository {

    private static final ProjectRepository INSTANCE = new ProjectRepository();

    public static ProjectRepository getInstance() {
        return INSTANCE;
    }

    private Map<String, Project> projects = new LinkedHashMap<>();

//    {
//        add(new Project("DEMO"));
//        add(new Project("TEST"));
//        add(new Project("MEGA"));
//        add(new Project("BETA"));
//    }

    public void create() {
        add(new Project("New project " + System.currentTimeMillis(), "pr_description"));
    }

    public void add(@NotNull final Project project) {
        projects.put(project.getId(), project);
    }

    public Project save(@NotNull final Project project) {
        final String id = project.getId();
        projects.put(id, project);
        return projects.get(id);
    }

    public Collection<Project> findAll() {
        return projects.values();
    }

    public Project findById(@NotNull final String id) {
        return projects.get(id);
    }

    public void removeById(@NotNull final String id) {
        projects.remove(id);
    }

}
