package ru.t1.chubarov.tm.api;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1.chubarov.tm.model.Task;

@Repository
public interface ITaskRepository extends JpaRepository<Task,String> {

}

