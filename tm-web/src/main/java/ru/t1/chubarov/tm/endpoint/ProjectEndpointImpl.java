package ru.t1.chubarov.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.chubarov.tm.api.IProjectEndpoint;
import ru.t1.chubarov.tm.model.Project;
import ru.t1.chubarov.tm.service.ProjectService;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;

@WebService(endpointInterface = "ru.t1.chubarov.tm.api.IProjectEndpoint")
@RestController
@RequestMapping("/api/projects")
public class ProjectEndpointImpl implements IProjectEndpoint {

    @Autowired
    private ProjectService projectService;

    @Override
    @WebMethod
    @GetMapping("/findAll")
    public Collection<Project> findAll() {
        return projectService.findAll();
    }

    @Override
    @WebMethod
    @PostMapping("/create")
    public Project create() {
        return projectService.create();
    }

    @Override
    @WebMethod
    @PostMapping("/add")
    public Project add(
            @WebParam(name = "project", partName = "project")
            @RequestBody final Project project) {
        return projectService.add(project);
    }

    @Override
    @WebMethod
    @PostMapping("/save")
    public Project save(
            @WebParam(name = "project", partName = "project")
            @RequestBody final Project project) {
        return projectService.save(project);
    }

    @Override
    @WebMethod
    @GetMapping("/findById/{id}")
    public Project findById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") final String id) {
        return projectService.findById(id);
    }

    @Override
    @WebMethod
    @PostMapping("/delete/{id}")
    public void delete(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") final String id) {
        projectService.removeById(id);
    }

}
