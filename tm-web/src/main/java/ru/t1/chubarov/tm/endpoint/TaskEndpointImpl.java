package ru.t1.chubarov.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.chubarov.tm.api.ITaskEndpoint;
import ru.t1.chubarov.tm.model.Task;
import ru.t1.chubarov.tm.service.TaskService;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;

@RestController
@RequestMapping("/api/tasks")
@WebService(endpointInterface = "ru.t1.chubarov.tm.api.ITaskEndpoint")
public class TaskEndpointImpl implements ITaskEndpoint {

    @Autowired
    TaskService taskService;

    @Override
    @WebMethod
    @PostMapping("/create")
    public Task create() {
        return taskService.create("NewTask", "NewDesc");
    }

    @Override
    @WebMethod
    @PostMapping("/add")
    public Task add(
            @WebParam(name = "task", partName = "task")
            @RequestBody final Task task) {
        return taskService.add(task);
    }

    @Override
    @WebMethod
    @GetMapping("/findAll")
    public Collection<Task> findAll() {
        return taskService.findAll();
    }

    @Override
    @WebMethod
    @PostMapping("/save")
    public Task save(
            @WebParam(name = "task", partName = "task")
            @RequestBody final Task task) {
        return taskService.save(task);
    }

    @Override
    @WebMethod
    @GetMapping("/findById/{id}")
    public Task findById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") final String id) {
        return taskService.findById(id);
    }

    @Override
    @WebMethod
    @PostMapping("/delete/{id}")
    public void delete(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") final String id) {
        taskService.removeById(id);
    }

}
