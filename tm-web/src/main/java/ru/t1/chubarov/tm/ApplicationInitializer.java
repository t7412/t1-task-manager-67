package ru.t1.chubarov.tm;


import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;
import ru.t1.chubarov.tm.config.ApplicationConfiguration;
import ru.t1.chubarov.tm.config.WebApplicationConfiguration;

//http://localhost:8080/ws/PojectEndpoint?wsdl
//http://localhost:8080/ws/TaskEndpoint?wsdl
public class ApplicationInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class[] {ApplicationConfiguration.class};
    }

    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class[]{
            WebApplicationConfiguration.class
        };
    }

    @Override
    protected String[] getServletMappings() {
        return new String[]{"/"};
    }

}
